#!/bin/sh
CURRENT_DIR=$(pwd)

# Stop old server
if [ -d "latest" ]; then
    cd latest

    if [ "stop.sh" ]; then
      echo "Stop Batch"

      ./stop.sh

      # Check stop batch
      if [ $? -eq 0 ]
      then
        echo "Batch stopped"
      else
        echo "Error when Batch stoped" >&2
      fi
    fi

    cd ..
    rm latest
fi

# If deploy same version, dir is deleted
if [ -d "${project.build.finalName}" ]; then
    echo "Same version exist, it is deleted."
    rm -rf ${project.build.finalName}
fi

# unzip new version
echo "Unzip Batch in $CURRENT_DIR"
unzip ${project.build.finalName}-run -d ${project.build.finalName}

# Recreate link to lastest
ln -s ${project.build.finalName} latest

# Launch JBoss
echo "Configure new version"
cd latest
# set execute mode
chmod -R u+x *.sh
if [ -d "bin" ]; then
  chmod -R u+x bin/*.sh
fi

# Check start JBoss
if [ $? -eq 0 ]
then
  echo "Successfully installed"
  exit 0
else
  echo "Problem occured: " >&2
  exit 1
fi
