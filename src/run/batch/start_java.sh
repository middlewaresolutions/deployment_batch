#!/bin/sh
export JAVA_HOME=${JAVA_HOME}
export JAVA=$JAVA_HOME/bin/java

export JAVA_OPTS="${JAVA_OPTS}"
export MAIN_CLASS=${MAIN_CLASS}
export TASK_CLASS=${TASK_CLASS}

# Services PENHIR2
export LOG_LEVEL=${LOG_LEVEL}

# Start batch
$JAVA_HOME/bin/java $JAVA_OPTS -DLOG_LEVEL=$LOG_LEVEL -cp .\lib $MAIN_CLASS $TASK_CLASS
